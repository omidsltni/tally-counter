package sbu.cs;

/**
 * you have nothing to do with this class
 * You'll learn about Interfaces later on in this course.
 * But if you are curious about Interfaces now, reach out your
 * mentor or visit the link written tutorial below.
 * @see <a href="https://www.geeksforgeeks.org/interfaces-in-java/">
 *   Interfaces in Java
 *   </a>
 */
public interface TallyCounterInterface {

    void count();
    int getValue();
    void setValue(int value);
    void reset();
}
